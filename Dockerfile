FROM golang:1-alpine AS builder

RUN echo "@testing https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk add --upgrade --no-cache curl bash build-base hurl@testing sqlite

WORKDIR /app

COPY cmd           ./cmd
COPY internal      ./internal
COPY go.mod go.sum .
RUN go build ./cmd/machinist

COPY e2e        ./e2e
COPY schema.sql .
RUN sqlite3 ./inventory.sql < schema.sql

CMD ["bash", "./e2e/run.sh"]


FROM alpine

RUN apk add --upgrade --no-cache libgcc # for sqlite

COPY --from=builder /app/machinist     /machinist
COPY --from=builder /app/inventory.sql /inventory.sql

ENTRYPOINT ["/machinist"]
