build: db
	docker compose build machinist

up: build
	docker compose up machinist

DB_FILE ?= inventory.sql
db:
	rm -rf ${DB_FILE}
	sqlite3 ${DB_FILE} < schema.sql

e2e: db
	docker compose build test
	docker compose run --rm test bash ./e2e/run.sh
