# machinist

Small machine inventory application to track devices assigned to employees.

- [Build](#build)
- [Run](#run)
- [Test](#test)
- [Database](#database)
- [HTTP API](#http-api)
- [Notifications](#notifications)
- [To Dos](#to-dos)

## Build

Use `go build ./cmd/machinist` to build for your local machine.

Use `make build` to build the application container.

## Run

Use `go run ./cmd/machinist` to run a local instance of the application only.

To run both the application container as well as an instance of greenbone/exercise-admin-notification, use `make run`.

## Test

To execute the end-to-end test suite, run `make e2e`. (You don't need to execute `make run` beforehand.)

Confirm in the logs of the greenbone/exercise-admin-notification container that a notification was received.

## Database

The application uses an SQLite database to persist any state. Set the `DB_FILE` environment variable to override the default `./inventory.sql` location.

The database schema is defined in [schema.sql](./schema.sql).

## HTTP API

The application offers a RESTful API to perform CRUD operations. By default, the application listens on all interfaces and port 9090. Set the `LISTEN_ADDRESS` environment variable to change this, for example `LISTEN_ADDRESS=10.0.0.1:443`.

See [http_api.md](./docs/http_api.md) for a detailed overview over HTTP endpoints.

## Notifications

If the `ADMIN_NOTIFY_ENDPOINT` environment variable is set, a JSON notification will be POSTed to that endpoint whenever an API operation results in three or more devices being assigned to the same employee. The notification will be of the following format:

```json
{
    "level": "warning",
    "employeeAbbreviation": "<shortcode>",
    "message": "employee <shortcode> has <num_devices> devices assigned"
}
```

`<shortcode>` is the employee's three-letter abbreviation, `<num_devices>` is the number of devices assigned to the employee at the time the notification was created.

If an employee is assigned a third device, a notification is created. If the same employee is assigned a fourth device, another notification will be created. If a device is removed from an employee, but they still have three or more devices assigned afterwards, a notification will be created as well.

## To Dos

See [TODO.md](./TODO.md) for a list of things that should be improved before using this in production.
