What should be improved:

- tests:
  - more tests, especially unhappy paths
  - unit tests if they make sense
  - some way to verify (in tests) that the notification service receives our POST calls
  - test DELETE endpoints
- payload validation, probably in a layer between handler and repository
- structured logging, see internal/server/create.go
- use actual ID column as DB primary keys
- graceful shutdown of notification sender goroutine
- use transaction or RETURNING instead of a SELECT after every device INSERT/UPDATE to check for num_devices > 3
- make Dockerfile play well with configuration env vars
- trigger alert when num_devices > 3 after DELETE /devices/{mac}
