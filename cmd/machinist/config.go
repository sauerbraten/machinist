package main

import (
	"os"

	"github.com/charmbracelet/log"
	"github.com/kelseyhightower/envconfig"
)

type config struct {
	LogLevel            string `split_words:"true" default:"info"`
	DBFile              string `split_words:"true" default:"./inventory.sql"`
	ListenAddress       string `split_words:"true" default:"0.0.0.0:9090"`
	AdminNotifyEndpoint string `split_words:"true"`
}

func loadConfig() config {
	conf := config{}

	err := envconfig.Process("", &conf)
	if err != nil {
		log.Fatal("loading config", "err", err)
		os.Exit(1)
	}

	return conf
}
