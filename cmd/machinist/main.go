package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/charmbracelet/log"

	"gitlab.com/sauerbraten/machinist/internal/notification"
	"gitlab.com/sauerbraten/machinist/internal/resource"
	"gitlab.com/sauerbraten/machinist/internal/server"
	"gitlab.com/sauerbraten/machinist/internal/sql"
)

func main() {
	// load config
	conf := loadConfig()

	// set up logging
	def := log.Default().With("app", "machinist")
	def.SetLevel(log.ParseLevel(conf.LogLevel))
	log.SetDefault(def)

	db, err := sql.NewSQLite(conf.DBFile)
	if err != nil {
		log.Error("connecting to database", "error", err)
		os.Exit(1)
	}

	// start notification sender
	notifier, alerts := notification.NewSender(conf.AdminNotifyEndpoint)
	go notifier.RunForever()

	devices := resource.NewDeviceRepository(db, "devices", "mac", alerts)
	employees := resource.NewEmployeeRepository(db, "employees", "shortcode")

	server := server.NewServer(conf.ListenAddress, devices, employees)

	// set up signal handling
	ctx, cancel := context.WithCancel(context.Background())

	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)
	go func() {
		s := <-signalCh
		log.Debug(fmt.Sprintf("received %s", s))
		cancel()
	}()

	go func() {
		log.Info(fmt.Sprintf("listening on %s", server.Addr))
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Error("running server", "error", err)
		}
	}()

	// wait for interrupt (e.g. Ctrl-C)
	<-ctx.Done()

	// gracefully shut down server
	log.Debug("shutting down server")

	shutdownCtx, cancelShutdown := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelShutdown()

	if err := server.Shutdown(shutdownCtx); err != nil {
		log.Error("shutting down server", "error", err)
	}

	log.Debug("server shut down gracefully")

	// close DB connection
	if err := db.Close(); err != nil {
		log.Error("closing DB connection", "error", err)
	}
}
