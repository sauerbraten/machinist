# HTTP API

The app offers a REST-like API over HTTP. There are two resource types, _devices_ and _employees_. For both, the API exposes CRUD operations as well as an endpoint to list all/multiple resources.

- [Devices](#devices)
  - [Create a device: POST /devices](#create-a-device-post-devices)
  - [List devices: GET /devices](#list-devices-get-devices)
  - [Read device: GET /devices/{mac}](#read-device-get-devicesmac)
  - [Update a device: PATCH /devices/{mac}](#update-a-device-patch-devicesmac)
  - [Delete a device: DELETE /devices/{mac}](#delete-a-device-delete-devicesmac)
- [Employees](#employees)
  - [Create an employee: POST /employees](#create-an-employee-post-employees)
  - [List employees: GET /employees](#list-employees-get-employees)
  - [Read employee: GET /employees/{shortcode}](#read-employee-get-employeesshortcode)
  - [Update an employee: PATCH /employees/{shortcode}](#update-an-employee-patch-employeesshortcode)
  - [Delete an employee: DELETE /employees/{shortcode}](#delete-an-employee-delete-employeesshortcode)

## Devices

### Create a device: POST /devices

Pass device data as JSON body, for example:

```json
{
    "mac": "1254D0F85E3E",
    "ip": "10.0.0.123",
    "name": "thinkpad",
    "description": "T480s / 2TB / Windows 10",
    "employee": "mmu"
}
```

All fields except `description` and `employee` are required.

Returns 201 Created if successful.

### List devices: GET /devices

Returns a list of all known devices as JSON, for example:

```json
[
    {
        "mac": "1254D0F85E3E",
        "ip": "10.1.2.3",
        "name": "thinkpad",
        "description": "T480s / 2TB / Windows 10",
        "employee": "mmu"
    },
    {
        "mac": "8F2CE5656D87",
        "ip": "10.4.5.6",
        "name": "macbook",
        "description": null,
        "employee": null
    }
]
```

Pass `?employee=<shortcode>` to only show devices assigned to that employee.

### Read device: GET /devices/{mac}

Returns 200 OK and a JSON body, for example:

```json
{
    "mac": "1254D0F85E3E",
    "ip": "10.0.0.123",
    "name": "thinkpad",
    "description": "T480s / 2TB / Windows 10",
    "employee": "mmu"
}
```

### Update a device: PATCH /devices/{mac}

Pass only the field(s) you want to update as JSON body, for example:

```json
{
    "ip": "10.0.0.234",
    "description": "T480s / 2TB / Windows 11"
}
```

Returns 200 OK and the udpated device data as JSON body, for example:

```json
{
    "mac": "1254D0F85E3E",
    "ip": "10.0.0.234",
    "name": "thinkpad",
    "description": "T480s / 2TB / Windows 11",
    "employee": "mmu"
}
```

Use this endpoint to assign a device to an employee. Send `{"employee": null}` to remove the device from the employee.

### Delete a device: DELETE /devices/{mac}

Returns 200 OK if successful.

## Employees

### Create an employee: POST /employees

Pass employee data as JSON body, for example:

```json
{
    "shortcode": "mmu",
    "name": "Max Mustermann"
}
```

Both fields are required.

Returns 201 Created if successful.

### List employees: GET /employees

Returns a list of all known employees as JSON, for example:

```json
[
    {
        "shortcode": "mmu",
        "name": "Max Mustermann"
    },
    {
        "shortcode": "fmu",
        "name": "Franziska Musterfrau"
    }
]
```

### Read employee: GET /employees/{shortcode}

Returns 200 OK and a JSON body, for example:

```json
{
    "shortcode": "mmu",
    "name": "Max Mustermann"
}
```

### Update an employee: PATCH /employees/{shortcode}

Pass only the field(s) you want to update as JSON body, for example:

```json
{
    "name": "Max Geheiratet"
}
```

Returns 200 OK and the udpated employee data as JSON body, for example:

```json
{
    "shortcode": "mmu",
    "name": "Max Geheiratet"
}
```

### Delete an employee: DELETE /employees/{shortcode}

Returns 200 OK if successful. Will fail with 409 Conflict if there are still devices assigned to the employee.
