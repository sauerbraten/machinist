#!/bin/bash
# set -o xtrace

cd $(dirname $0)/..

# run application in the background
./machinist &
pid=$!

# wait for API to become ready
echo "waiting for API to be ready"
while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:9090/health)" != "200" ]]; do sleep 2; done

# run happy path tests
hurl --test \
    --variable name='Max Mustermann' \
    --variable shortcode='mmu' \
    --variable mac='1254D0F85E3E' \
    --variable mac2='8F2CE5656D87' \
    --variable mac3='CD52D7A4D0A8' \
    ./e2e/happy_path.hurl

# shutdown API
kill -INT $pid
wait
