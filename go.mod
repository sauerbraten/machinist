module gitlab.com/sauerbraten/machinist

go 1.20

require (
	github.com/charmbracelet/log v0.2.2
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/jba/muxpatterns v0.2.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mattn/go-sqlite3 v1.14.17
	golang.org/x/exp v0.0.0-20230519143937-03e91628a987
)

require (
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/lipgloss v0.7.1 // indirect
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
