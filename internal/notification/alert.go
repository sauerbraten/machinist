package notification

import (
	"encoding/json"
	"fmt"
)

type Alert struct {
	Employee   string
	NumDevices int
}

func (a Alert) MarshalJSON() ([]byte, error) {
	return json.Marshal(
		map[string]any{
			"level":                "warning",
			"employeeAbbreviation": a.Employee,
			"message":              fmt.Sprintf("employee %s has %d devices assigned", a.Employee, a.NumDevices),
		},
	)
}
