package notification

import (
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/charmbracelet/log"
)

type Sender struct {
	alerts   <-chan Alert
	endpoint string
	// todo: stop channel or context for graceful shutdown?
}

func NewSender(endpoint string) (Sender, chan<- Alert) {
	ch := make(chan Alert)
	return Sender{
		alerts:   ch,
		endpoint: endpoint,
	}, ch
}

func (s *Sender) RunForever() {
	l := log.With("endpoint", s.endpoint)
	l.Debug("running notification sender")

	for a := range s.alerts {
		l = l.With("alert", a)

		if s.endpoint == "" {
			// discard
			l.Debug("discarding alert because no endpoint is configured")
			continue
		}

		body, err := json.Marshal(a)
		if err != nil {
			l.Error("marshaling alert to JSON", "error", err)
			continue
		}

		l.Debug("POSTing alert to notification service")

		resp, err := http.Post(s.endpoint, "application/json", bytes.NewReader(body))
		if err != nil {
			l.Error("POSTing alert to endpoint", "error", err)
			continue
		}

		if resp.StatusCode != http.StatusOK {
			l.Error("unexpected status from notification endpoint", "expected", http.StatusOK, "actual", resp.StatusCode)
		}
	}
}
