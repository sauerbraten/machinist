package resource

import (
	"github.com/charmbracelet/log"
	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"

	"gitlab.com/sauerbraten/machinist/internal/notification"
	"gitlab.com/sauerbraten/machinist/internal/sql"
)

type Device struct {
	MAC         string  `json:"mac"`
	IP          string  `json:"ip"`
	Name        string  `json:"name"`
	Description *string `json:"description,omitempty"`
	Employee    *string `json:"employee,omitempty"`
}

type DeviceUpdate struct {
	// assumption: MAC can not be changed, as it's the devices unique identifier (and primary key in DB)

	IP          *string `json:"ip,omitempty"`
	Name        *string `json:"name,omitempty"`
	Description *string `json:"description,omitempty"`
	Employee    *string `json:"employee,omitempty"`
}

type DeviceRepository struct {
	Repository[Device, DeviceUpdate]
	alerts chan<- notification.Alert
}

func NewDeviceRepository(db sql.Database, table, idColumn string, alerts chan<- notification.Alert) *DeviceRepository {
	repo := NewRepository[Device, DeviceUpdate](db, table, idColumn)

	return &DeviceRepository{
		Repository: *repo,
		alerts:     alerts,
	}
}

func (r *DeviceRepository) Create(in Device) (*Device, error) {
	out, err := r.Repository.Create(in)
	if err != nil {
		return nil, err
	}
	if in.Employee != nil {
		go r.checkNumDevices(*in.Employee)
	}
	return out, nil
}

func (r *DeviceRepository) Patch(id string, upd DeviceUpdate) (*Device, error) {
	out, err := r.Repository.Patch(id, upd)
	if err != nil {
		return nil, err
	}
	if upd.Employee != nil {
		go r.checkNumDevices(*upd.Employee)
	}
	return out, nil
}

func (r *DeviceRepository) checkNumDevices(employee string) {
	employeeCol := goqu.C("employee")
	query, args, err := r.db.Q().
		Select(goqu.COUNT(employeeCol)).
		From(r.table).
		Where(employeeCol.Eq(employee)).
		GroupBy(employeeCol).
		ToSQL()
	if err != nil {
		log.Error("building query to count devices assigned to employee", "shortcode", employee, "err", err)
	}

	count := 0
	err = sqlx.Get(r.db, &count, query, args...)
	if err != nil {
		log.Error("running query to count devices assigned to employee", "shortcode", employee, "err", err)
	}

	if count >= 3 {
		r.alerts <- notification.Alert{
			Employee:   employee,
			NumDevices: count,
		}
	}
}
