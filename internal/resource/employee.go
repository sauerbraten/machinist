package resource

type Employee struct {
	Shortcode string `json:"shortcode"`
	Name      string `json:"name"`
}

type EmployeeUpdate struct {
	// assumption: shortcode can not be changed, as it's the employees unique identifier (and primary key in DB)

	Name *string `json:"name,omitempty"`
}

type EmployeeRepository = Repository[Employee, EmployeeUpdate]

var NewEmployeeRepository = NewRepository[Employee, EmployeeUpdate]
