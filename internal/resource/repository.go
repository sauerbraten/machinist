package resource

import (
	"encoding/json"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"

	"gitlab.com/sauerbraten/machinist/internal/sql"
)

type Repository[Resource, Patch any] struct {
	db       sql.Database
	table    exp.IdentifierExpression
	idColumn exp.IdentifierExpression
}

func NewRepository[Resource, Patch any](db sql.Database, table, idColumn string) *Repository[Resource, Patch] {
	return &Repository[Resource, Patch]{
		db:       db,
		table:    goqu.T(table),
		idColumn: goqu.C(idColumn),
	}
}

func (r *Repository[Resource, Patch]) Create(res Resource) (*Resource, error) {
	err := sql.Create(r.db, r.table, res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (r *Repository[Resource, Patch]) List(wheres ...goqu.Expression) ([]*Resource, error) {
	return sql.List[Resource](r.db, r.table, wheres...)
}

func (r *Repository[Resource, Patch]) Read(id string) (*Resource, error) {
	return sql.Read[Resource](r.db, r.table, r.idColumn.Eq(id))
}

func (r *Repository[Resource, Patch]) update(id string, res Resource) (*Resource, error) {
	err := sql.Update(r.db, r.table, res, r.idColumn.Eq(id))
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (r *Repository[Resource, Patch]) Patch(id string, upd Patch) (*Resource, error) {
	res, err := r.Read(id)
	if err != nil {
		return nil, err
	}

	// convert update to JSON
	j, err := json.Marshal(upd)
	if err != nil {
		return nil, err
	}

	// apply values from JSON onto current values
	err = json.Unmarshal(j, res)
	if err != nil {
		return nil, err
	}

	return r.update(id, *res)
}

func (r *Repository[Resource, Patch]) Delete(id string) error {
	return sql.Delete(r.db, r.table, r.idColumn.Eq(id))
}
