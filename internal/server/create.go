package server

import (
	"fmt"
	"net/http"

	"github.com/charmbracelet/log"

	"gitlab.com/sauerbraten/machinist/internal/resource"
	"gitlab.com/sauerbraten/machinist/internal/server/json"
)

type creater[Resource any] interface {
	Create(Resource) (*Resource, error)
}

func create[Resource any](service creater[Resource]) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := log.With("http_method", r.Method, "http_path", r.URL.Path) // todo: do this in all handlers

		t := new(Resource)

		err := json.Decode(r.Body, t)
		if err != nil {
			l.Error("reading resource from body", "error", err)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, err.Error())
			return
		}

		resource, err := service.Create(*t)
		if err != nil {
			l.Error("creating resource", "error", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err.Error())
			return
		}

		err = json.Write(w, http.StatusCreated, resource)
		if err != nil {
			l.Error("writing resource to HTTP response", "error", err)
			return
		}
	}
}

var (
	createDevice   = create[resource.Device]
	createEmployee = create[resource.Employee]
)
