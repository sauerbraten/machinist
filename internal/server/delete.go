package server

import (
	"fmt"
	"net/http"

	"github.com/charmbracelet/log"

	"gitlab.com/sauerbraten/machinist/internal/resource"
)

type deleter[Resource any] interface {
	Delete(id string) error
}

func del[Resource any](service deleter[Resource], getID func(r *http.Request) string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := getID(r)

		err := service.Delete(id)
		if err != nil {
			log.Error("deleting resource", "error", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err.Error())
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

var (
	deleteDevice   = del[resource.Device]
	deleteEmployee = del[resource.Employee]
)
