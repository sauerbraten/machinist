package server

import (
	"fmt"
	"net/http"

	"github.com/charmbracelet/log"
	"github.com/doug-martin/goqu/v9"
	"golang.org/x/exp/slices"

	"gitlab.com/sauerbraten/machinist/internal/resource"
	"gitlab.com/sauerbraten/machinist/internal/server/json"
)

type lister[Resource any] interface {
	List(wheres ...goqu.Expression) ([]*Resource, error)
}

func list[Resource any](repo lister[Resource], allowedFilters ...string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		wheres := []goqu.Expression{}
		for param, values := range r.URL.Query() {
			if slices.Contains(allowedFilters, param) {
				wheres = append(wheres, goqu.C(param).In(values))
			}
		}

		resources, err := repo.List(wheres...)
		if err != nil {
			log.Error("listing resources", "error", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err.Error())
			return
		}

		err = json.Write(w, http.StatusOK, resources)
		if err != nil {
			log.Error("writing resource to HTTP response", "error", err)
			return
		}
	}
}

var (
	listDevice   = list[resource.Device]
	listEmployee = list[resource.Employee]
)
