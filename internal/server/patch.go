package server

import (
	"fmt"
	"net/http"

	"github.com/charmbracelet/log"

	"gitlab.com/sauerbraten/machinist/internal/resource"
	"gitlab.com/sauerbraten/machinist/internal/server/json"
)

type patcher[Resource, Patch any] interface {
	Patch(id string, patch Patch) (*Resource, error)
}

func patch[R, P any](service patcher[R, P], getID func(r *http.Request) string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := getID(r)

		patch := new(P)
		err := json.Decode(r.Body, patch)
		if err != nil {
			log.Error("reading resource patch from body", "error", err)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(w, err.Error())
			return
		}

		ent, err := service.Patch(id, *patch)
		if err != nil {
			log.Error("patching resource", "error", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err.Error())
			return
		}

		err = json.Write(w, http.StatusOK, ent)
		if err != nil {
			log.Error("writing resource to HTTP response", "error", err)
			return
		}
	}
}

var (
	patchDevice   = patch[resource.Device, resource.DeviceUpdate]
	patchEmployee = patch[resource.Employee, resource.EmployeeUpdate]
)
