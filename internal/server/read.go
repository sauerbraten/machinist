package server

import (
	"fmt"
	"net/http"

	"github.com/charmbracelet/log"

	"gitlab.com/sauerbraten/machinist/internal/resource"
	"gitlab.com/sauerbraten/machinist/internal/server/json"
)

type reader[Resource any] interface {
	Read(id string) (*Resource, error)
}

func read[R any](service reader[R], getID func(r *http.Request) string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := getID(r)

		resource, err := service.Read(id)
		if err != nil {
			log.Error("reading resource", "error", err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, err.Error())
			return
		}

		err = json.Write(w, http.StatusOK, resource)
		if err != nil {
			log.Error("writing resource to HTTP response", "error", err)
			return
		}
	}
}

var (
	readDevice   = read[resource.Device]
	readEmployee = read[resource.Employee]
)
