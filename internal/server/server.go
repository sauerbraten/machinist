package server

import (
	"fmt"
	"net/http"

	"github.com/jba/muxpatterns"

	"gitlab.com/sauerbraten/machinist/internal/resource"
)

type Server struct {
	http.Server
	devices   *resource.DeviceRepository
	employees *resource.EmployeeRepository
}

func NewServer(
	listenAddr string,
	devices *resource.DeviceRepository,
	employees *resource.EmployeeRepository,
) *Server {
	s := &Server{
		devices:   devices,
		employees: employees,
	}

	mux := muxpatterns.NewServeMux()

	mux.HandleFunc("GET /health", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `{"status": "ok"}`)
	})

	getMAC := func(r *http.Request) string { return mux.PathValue(r, "mac") }
	mux.HandleFunc("POST /devices", createDevice(s.devices))
	mux.HandleFunc("GET /devices", listDevice(s.devices, "employee"))
	mux.HandleFunc("GET /devices/{mac}", readDevice(s.devices, getMAC))
	mux.HandleFunc("PATCH /devices/{mac}", patchDevice(s.devices, getMAC))
	mux.HandleFunc("DELETE /devices/{mac}", deleteDevice(s.devices, getMAC))

	getShortcode := func(r *http.Request) string { return mux.PathValue(r, "shortcode") }
	mux.HandleFunc("POST /employees", createEmployee(s.employees))
	mux.HandleFunc("GET /employees", listEmployee(s.employees))
	mux.HandleFunc("GET /employees/{shortcode}", readEmployee(s.employees, getShortcode))
	mux.HandleFunc("PATCH /employees/{shortcode}", patchEmployee(s.employees, getShortcode))
	mux.HandleFunc("DELETE /employees/{shortcode}", deleteEmployee(s.employees, getShortcode))

	s.Server = http.Server{
		Addr:    listenAddr,
		Handler: mux,
	}

	return s
}
