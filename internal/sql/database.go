package sql

import (
	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
)

// Database can build and run queries, and close its connection.
type Database interface {
	Q() *goqu.SelectDataset
	sqlx.Execer
	sqlx.Queryer
	Close() error
}
