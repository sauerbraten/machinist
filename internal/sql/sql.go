package sql

import (
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jmoiron/sqlx"
)

// generic create/list/read/update/delete code that works on any implementation of Database

func Create(db Database, table exp.IdentifierExpression, row any) error {
	query, args, err := db.Q().
		Insert().
		Into(table).
		Rows(row).
		ToSQL()
	if err != nil {
		return err
	}

	_, err = db.Exec(query, args...)
	return err
}

func List[T any](db Database, table exp.IdentifierExpression, wheres ...exp.Expression) ([]*T, error) {
	query, args, err := db.Q().
		Select().
		From(table).
		Where(wheres...).
		ToSQL()
	if err != nil {
		return nil, err
	}

	ts := make([]*T, 0)
	err = sqlx.Select(db, &ts, query, args...)
	if err != nil {
		return nil, err
	}

	return ts, nil
}

func Read[T any](db Database, table exp.IdentifierExpression, wheres ...exp.Expression) (*T, error) {
	query, args, err := db.Q().
		Select().
		From(table).
		Where(wheres...).
		ToSQL()
	if err != nil {
		return nil, err
	}

	t := new(T)
	err = sqlx.Get(db, t, query, args...)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func Update(db Database, table exp.IdentifierExpression, row any, wheres ...exp.Expression) error {
	query, args, err := db.Q().
		Update().
		Table(table).
		Set(row).
		Where(wheres...).
		ToSQL()
	if err != nil {
		return err
	}

	_, err = db.Exec(query, args...)
	return err
}

func Delete(db Database, table exp.IdentifierExpression, wheres ...exp.Expression) error {
	query, args, err := db.Q().
		Delete().
		From(table).
		Where(wheres...).
		ToSQL()
	if err != nil {
		return err
	}

	_, err = db.Exec(query, args...)
	return err
}
