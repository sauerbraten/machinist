package sql

import (
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type sqliteQueryBuilder struct {
	//
}

func (*sqliteQueryBuilder) Q() *goqu.SelectDataset {
	return goqu.Dialect("sqlite3").Select().Prepared(true)
}

type sqliteDatabase struct {
	sqliteQueryBuilder
	*sqlx.DB
}

func NewSQLite(path string) (Database, error) {
	db, err := sqlx.Open("sqlite3", path)
	if err != nil {
		return nil, fmt.Errorf("db: sqlite: opening database file: %w", err)
	}

	_, err = db.Exec("pragma foreign_keys = on")
	if err != nil {
		return nil, fmt.Errorf("db: sqlite: enabling foreign keys: %w", err)
	}

	_, err = db.Exec("pragma journal_mode = wal")
	if err != nil {
		return nil, fmt.Errorf("db: sqlite: enabling WAL journaling: %w", err)
	}

	return &sqliteDatabase{sqliteQueryBuilder{}, db}, nil
}
