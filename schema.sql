pragma foreign_keys = on;

create table employees (
    shortcode text primary key,
    name text not null
);

create table devices (
    mac text primary key, -- if slow, change to integer
    ip text not null, -- if slow, change to integer
    name text not null,
    description text,
    employee text references employees(shortcode)
);

pragma quick_check;
pragma foreign_key_check;
